<?php

namespace App\Repository;

use App\Entity\ParticipantMakeEventCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ParticipantMakeEventCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParticipantMakeEventCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParticipantMakeEventCourse[]    findAll()
 * @method ParticipantMakeEventCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipantMakeEventCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParticipantMakeEventCourse::class);
    }
}
