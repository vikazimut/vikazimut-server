<?php

namespace App\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use App\Repository\CourseRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use stdClass;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: CourseRepository::class)]
class Course
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: "courses")]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $creator;

    #[ORM\Column(type: Types::TEXT, length: 65535)]
    private ?string $xml;

    #[ORM\Column(type: Types::TEXT, length: 255)]
    private ?string $image;

    #[ORM\Column(type: Types::STRING, length: 2000)]
    private ?string $kml;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $createdAt;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $name;

    #[ORM\OneToMany(targetEntity: Track::class, mappedBy: "course", orphanRemoval: true)]
    private Collection $orienteer;

    #[ORM\OneToMany(targetEntity: MissingControlPoint::class, mappedBy: "course", orphanRemoval: true)]
    private Collection $missingControlPoints;

    #[ORM\Column(type: Types::STRING)]
    private ?string $latitude;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private ?string $longitude;

    #[ORM\Column(type: Types::INTEGER)]
    private ?int $length;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $printable = false;

    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $detectionRadius;

    #[ORM\Column(type: Types::STRING, nullable: true)]
    private ?string $club;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $startDate = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $endDate = null;

    #[ORM\Column(type: Types::INTEGER)]
    private ?int $downloads = 0;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    private ?string $clubURL;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $touristic;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    private ?int $discipline;

    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $course_type;

    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $course_format;

    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $course_mode;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
    #[Encrypted]
    private ?string $secretKey = null;

    #[ORM\Column(type: Types::BOOLEAN, options: ["default" => false])]
    private bool $hidden = false;

    private int $orienteerCount = 0;

    public function __construct()
    {
        $this->orienteer = new ArrayCollection();
        $this->missingControlPoints = new ArrayCollection();
    }

    public function remove(EntityManagerInterface $entityManager): void
    {
        if (file_exists($this->image)) {
            unlink($this->image);
        }
        $tracks = $entityManager->getRepository(Track::class)->findByCourse($this->getId());
        /** @var Track $track */
        foreach ($tracks as $track) {
            $entityManager->remove($track);
        }
        $lives = $entityManager->getRepository(LiveTracking::class)->findByCourse($this->getId());
        /** @var LiveTracking $live */
        foreach ($lives as $live) {
            $live->remove($entityManager);
        }
        $missingFlags = $this->getMissingControlPoints();
        foreach ($missingFlags as $missingControlPoint) {
            $entityManager->remove($missingControlPoint);
        }
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getXml(): ?string
    {
        return $this->xml;
    }

    public function setXml(string $xml): self
    {
        $this->xml = $xml;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getKml(): ?string
    {
        return $this->kml;
    }

    public function setKml(string $kml): self
    {
        $this->kml = $kml;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getClub(): ?string
    {
        return $this->club;
    }

    public function setClub(?string $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getClubURL(): ?string
    {
        return $this->clubURL;
    }

    public function setClubURL(?string $clubURL): self
    {
        $this->clubURL = $clubURL;

        return $this;
    }

    public function getTouristic(): ?bool
    {
        return $this->touristic;
    }

    public function setTouristic(?bool $touristic): self
    {
        $this->touristic = $touristic;

        return $this;
    }

    /** @noinspection PhpUnused */
    public function getOrienteer(): Collection
    {
        return $this->orienteer;
    }

    public function getMissingControlPoints(): Collection
    {
        return $this->missingControlPoints;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength(int $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getPrintable(): ?bool
    {
        return $this->printable;
    }

    public function setPrintable(bool $printable): self
    {
        $this->printable = $printable;

        return $this;
    }

    public function getDetectionRadius(): ?int
    {
        return $this->detectionRadius;
    }

    /** @noinspection PhpUnused */
    public function setDetectionRadius(?int $radius): self
    {
        $this->detectionRadius = $radius;

        return $this;
    }

    /** @noinspection PhpUnused */
    public function detectionRadiusToString(int $radius): string
    {
        return (string)$radius;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getDownloads(): ?int
    {
        return $this->downloads;
    }

    public function setDownloads(int $downloads): self
    {
        $this->downloads = $downloads;

        return $this;
    }

    public function isHidden(): ?bool
    {
        return $this->hidden;
    }

    public function setHidden(?bool $hidden): self
    {
        $this->hidden = $hidden;

        return $this;
    }

    public function isOpen(): bool
    {
        return !$this->isClosed();
    }

    public function isClosed(): bool
    {
        $startingDate = $this->getStartDate();
        if ($startingDate !== null) {
            $now = new DateTime('now');
            if ($now < $startingDate) {
                return true;
            } else {
                $endDate = $this->getEndDate();
                if ($endDate !== null && $now > $endDate) {
                    return true;
                }
            }
        }
        return false;
    }

    static function extractCheckpointsFromXML($xmlRoute): array
    {
        $checkpoints = array();
        $i = 0;
        foreach ($xmlRoute->RaceCourseData->Course->CourseControl as $controlPoint) {
            $checkpointId = (string)$controlPoint->Control;
            if (isset($controlPoint->Score)) {
                $score = (int)$controlPoint->Score;
            } else {
                $score = 1;
            }
            foreach ($xmlRoute->RaceCourseData->Control as $control) {
                if ($checkpointId === (string)$control->Id) {
                    $obj = new stdClass();
                    $obj->location = array((float)$control->Position["lat"], (float)$control->Position["lng"]);
                    $obj->id = $checkpointId;
                    $obj->index = $i++;
                    $obj->type = (string)$controlPoint["type"];
                    $obj->score = $score;
                    $checkpoints[] = $obj;
                    break;
                }
            }
        }
        return $checkpoints;
    }

    public function getDiscipline(): ?int
    {
        return $this->discipline;
    }

    /** @noinspection PhpUnused */
    public function setDiscipline(?int $discipline): self
    {
        $this->discipline = $discipline;

        return $this;
    }

    /** @noinspection PhpUnused */
    public function disciplineToString($discipline): string
    {
        return match ($discipline) {
            1 => "foresto",
            2 => 'mtbo',
            3 => 'skio',
            default => "urbano",
        };
    }

    public
    function getCourseType(): ?int
    {
        return $this->course_type;
    }

    /** @noinspection PhpUnused */
    public function setCourseType(?int $course_type): self
    {
        $this->course_type = $course_type;

        return $this;
    }

    /** @noinspection PhpUnused */
    public function courseTypeToString($courseType): string
    {
        return match ($courseType) {
            1 => "geocaching",
            2 => "walk",
            default => "sport",
        };
    }

    public function getCourseFormat(): ?int
    {
        return $this->course_format;
    }

    /** @noinspection PhpUnused */
    public function setCourseFormat(?int $course_format): self
    {
        $this->course_format = $course_format;

        return $this;
    }

    /** @noinspection PhpUnused */
    public function courseFormatToString($courseFormat): string
    {
        return match ($courseFormat) {
            1 => "free",
            2 => "unsupervised",
            default => "preset",
        };
    }

    public function getCourseMode(): ?int
    {
        return $this->course_mode;
    }

    /** @noinspection PhpUnused */
    public function setCourseMode(?int $course_mode): self
    {
        $this->course_mode = $course_mode;

        return $this;
    }

    /** @noinspection PhpUnused */
    public function courseModeToString($courseMode): string
    {
        return match ($courseMode) {
            1 => "Manual",
            2 => "Beacon",
            default => "GPS",
        };
    }

    public function getSecretKey(): ?string
    {
        return $this->secretKey;
    }

    public function setSecretKey(?string $value): self
    {
        $this->secretKey = $value;

        return $this;
    }

    public function isPrivate(): bool
    {
        return $this->secretKey != null;
    }

    public function checkSecretKey(string $key): bool
    {
        return !strcmp($key, $this->secretKey);
    }

    public function getOrienteerCount(): int
    {
        return $this->orienteerCount;
    }

    public function setOrienteerCount(int $count): self
    {
        $this->orienteerCount = $count;
        return $this;
    }
}