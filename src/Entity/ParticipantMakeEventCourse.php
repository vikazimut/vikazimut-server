<?php

namespace App\Entity;

use App\Repository\ParticipantMakeEventCourseRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ParticipantMakeEventCourseRepository::class)]
class ParticipantMakeEventCourse
{
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: EventCourse::class)]
    private ?EventCourse $eventCourse;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Participant::class)]
    private ?Participant $participant;

    #[ORM\ManyToOne(targetEntity: Track::class)]
    #[ORM\JoinColumn(nullable: true)]
    private ?Track $track;

    #[ORM\Column(type: Types::INTEGER)]
    private int $score = 0;

    #[ORM\Column(type: Types::INTEGER)]
    private int $overtimeCount = 0;

    #[ORM\Column(type: Types::INTEGER)]
    private int $mispunchCount = 0;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $modified = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $mispunchPenaltyManuallySet = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $overtimePenaltyManuallySet = false;

    public function getEventCourse(): ?EventCourse
    {
        return $this->eventCourse;
    }

    public function setEventCourse(?EventCourse $eventCourse): self
    {
        $this->eventCourse = $eventCourse;

        return $this;
    }

    public function getParticipant(): ?Participant
    {
        return $this->participant;
    }

    public function setParticipant(?Participant $participant): self
    {
        $this->participant = $participant;

        return $this;
    }

    public function getTrack(): ?Track
    {
        return $this->track;
    }

    public function setTrack(?Track $track): self
    {
        $this->track = $track;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getOvertimeCount(): ?int
    {
        return $this->overtimeCount;
    }

    public function setOvertimeCount(int $overtimeCount): self
    {
        $this->overtimeCount = $overtimeCount;

        return $this;
    }

    public function getMispunchCount(): ?int
    {
        return $this->mispunchCount;
    }

    public function setMispunchCount(int $mispunchCount): self
    {
        $this->mispunchCount = $mispunchCount;

        return $this;
    }

    public function isModified(): bool
    {
        return $this->modified;
    }

    public function setModified(bool $modified): void
    {
        $this->modified = $modified;
    }

    public function isMispunchPenaltyManuallySet(): bool
    {
        return $this->mispunchPenaltyManuallySet;
    }

    public function setMispunchPenaltyManuallySet(bool $mispunchPenaltyManuallySet): void
    {
        $this->mispunchPenaltyManuallySet = $mispunchPenaltyManuallySet;
    }

    public function isOvertimePenaltyManuallySet(): bool
    {
        return $this->overtimePenaltyManuallySet;
    }

    public function setOvertimePenaltyManuallySet(bool $overtimePenaltyManuallySet): void
    {
        $this->overtimePenaltyManuallySet = $overtimePenaltyManuallySet;
    }

    public function remove(EntityManagerInterface $entityManager): void
    {
        $entityManager->remove($this);
        $entityManager->flush();
    }

    public function updatePenaltiesAndPointsFromTrack(): void
    {
        if ($this->track !== null) {
            $score = 0;
            if (!$this->isOvertimePenaltyManuallySet()) {
                $this->overtimeCount = $this->_computeOvertimeCount($this->track->getTotalTime(), $this->eventCourse->getMaxTime());
            }
            if (!$this->isMispunchPenaltyManuallySet()) {
                $this->mispunchCount = $this->_computeMispunchCount($this->track->getPunches());
            }
            if ($this->getParticipant()->getEvent()->getType() === POINT_EVENT_TYPE) {
                // Compute the number of points
                $xmlContents = simplexml_load_string($this->eventCourse->getCourse()->getXml());
                $controlPointIndex = 0;
                foreach ($xmlContents->RaceCourseData->Course->CourseControl as $y) {
                    // Do not use the start and end checkpoints
                    if ((string)$y->attributes()["type"] === "Control") {
                        if ((int)$this->track->getPunches()[$controlPointIndex]["punchTime"] > 0) {
                            if (isset($y->Score)) {
                                $score += (int)$y->Score;
                            } else {
                                $score += 1;
                            }
                        }
                    }
                    $controlPointIndex++;
                }
                $totalPenalty = $this->getMispunchCount() * $this->eventCourse->getMispunchPenalty()
                    + $this->getOvertimeCount() * $this->eventCourse->getOvertimePenalty();
                $score -= $totalPenalty;
                $this->setScore($score);
            }
        }
    }

    static public function _computeOvertimeCount($time, $timeLimit): int
    {
        $penalty = 0;
        if ($timeLimit > 0) {
            $overTime = $time - $timeLimit;
            if ($overTime > 0) {
                $overtimeInMinute = floor(($overTime / 1000) / 60);
                $penalty += $overtimeInMinute;
                if ($overtimeInMinute * 60 != $overTime / 1000) {
                    $penalty += 1;
                }
            }
        }
        return $penalty;
    }

    private function _computeMispunchCount($controlPoints): int
    {
        $mispunchCount = 0;
        foreach ($controlPoints as $controlPoint) {
            if ($controlPoint["punchTime"] <= 0 and $controlPoint["controlPoint"] !== 0) {
                $mispunchCount++;
            }
        }
        return $mispunchCount;
    }
}
