<?php

namespace App\Model;

use App\Entity\EventCourse;
use Doctrine\ORM\EntityManagerInterface;

class ModifyEventCourse
{
    private ?int $id;
    private ?int $format;
    private ?int $missingPunchPenalty;
    private ?int $overTimePenalty;
    private string $maxTimeInMilliseconds;

    public function __construct(EventCourse $eventCourse)
    {
        $this->id = $eventCourse->getId();
        $this->format = $eventCourse->getFormat();
        $this->missingPunchPenalty = $eventCourse->getMispunchPenalty();
        $this->overTimePenalty = $eventCourse->getOvertimePenalty();
        $this->maxTimeInMilliseconds = $eventCourse->getMaxTime();
    }

    function modify(EntityManagerInterface $entityManager): void
    {
        $eventCourse = $entityManager->getRepository(EventCourse::class)->find($this->id);
        $eventCourse->setFormat($this->format);
        $eventCourse->setMispunchPenalty($this->missingPunchPenalty);
        $eventCourse->setOvertimePenalty($this->overTimePenalty);
        $eventCourse->setMaxTime($this->maxTimeInMilliseconds);
        $entityManager->persist($eventCourse);
        $entityManager->flush();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function setFormat(?int $format): void
    {
        $this->format = $format;
    }

    public function setMissingPunchPenalty(?int $missingPunchPenalty): void
    {
        $this->missingPunchPenalty = $missingPunchPenalty;
    }

    public function setOverTimePenalty(?int $overTimePenalty): void
    {
        $this->overTimePenalty = $overTimePenalty;
    }

    public function setMaxTimeInMilliseconds(int|string $maxTimeInMilliseconds): void
    {
        $this->maxTimeInMilliseconds = $maxTimeInMilliseconds;
    }
}