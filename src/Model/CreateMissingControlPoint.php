<?php

namespace App\Model;

use App\Entity\Course;
use App\Entity\MissingControlPoint;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class CreateMissingControlPoint
{
    private MissingControlPoint $controlPoint;
    private mixed $data;

    public function __construct(mixed $data)
    {
        $this->controlPoint = new MissingControlPoint();
        $this->data = $data;
    }

    public function addControlPoint(EntityManagerInterface $entityManager): bool
    {
        if (!filter_var($this->data->courseId, FILTER_VALIDATE_INT) && $this->data->controlPointId == null) {
            return false;
        }
        $course = $entityManager->getRepository(Course::class)->find(intval($this->data->courseId));
        if ($course == null) {
            return false;
        }
        $this->controlPoint->setControlPointId($this->data->controlPointId);
        $this->controlPoint->setCourse($course);
        $entity = $entityManager->getRepository(MissingControlPoint::class)->findOneBy(
            [
                'controlPointId' => $this->data->controlPointId,
                'courseId' => $this->controlPoint->getCourse()->getId(),
            ]
        );
        $isDuplicated = !empty($entity);
        if ($isDuplicated) {
            return true;
        }
        try {
            $entityManager->persist($this->controlPoint);
            $entityManager->flush();
        } catch (Exception) {
            return false;
        }

        return true;
    }
}