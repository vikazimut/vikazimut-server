<?php

namespace App\Model;

use App\Entity\Course;
use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMElement;
use DOMException;
use Exception;
use SimpleXMLElement;
use function in_array;
use function simplexml_load_string;

class CreateCourse
{
    private Course $course;
    private User $user;
    private int $id;

    /**
     * @throws Exception
     */
    public function __construct(User $user, int $courseId, $repository)
    {
        $this->id = $courseId;
        $this->user = $user;
        if ($this->id > -1) {
            $course = $repository->find($this->id);
            if ($course == null) {
                throw new Exception("Bad request value");
            }
            $this->course = $course;
            if (!in_array($this->course, $this->user->getCourses()->toArray()) && !$this->user->isAdmin()) {
                throw new Exception("Unauthorized request");
            }
        } else {
            $this->course = new Course();
        }
    }

    /**
     * @throws CreateCourseException
     */
    public function processData($jsonData, $imageData, EntityManagerInterface $entityManager): int
    {
        $name = $jsonData["name"];
        $startDate = $jsonData["start_date"];
        $endDate = $jsonData["end_date"];
        $printable = $jsonData["is_printable"];
        $clubName = $jsonData["club_name"];
        $clubUrl = $jsonData["club_url"];
        $secretKey = $jsonData["secret_key"];
        $xml = $jsonData["xml"];
        $kml = $jsonData["kml"];
        $imageFilename = $jsonData["image"];
        $selectedXmlCourse = $jsonData["selected_xml_course"];

        $isCreationOfNewCourse = $this->id == -1;
        $errors = [];
        if ($isCreationOfNewCourse) {
            $this->course->setCreatedAt(new DateTime('now'));
            if ($name == null) {
                $errors[] = CourseValidator::ERROR_COURSE_EMPTY;
            }
            if ($xml == null) {
                $errors[] = CourseValidator::ERROR_XML_EMPTY;
            }
            if ($kml == null) {
                $errors[] = CourseValidator::ERROR_KML_EMPTY;
            }
            if ($imageData == null) {
                $errors[] = CourseValidator::ERROR_IMAGE_NOT_FOUND;
            }
            if (count($errors) > 0) {
                throw new CreateCourseException($errors);
            }

            $this->course->setName($name);
            if (!empty($clubName)) {
                $this->course->setClub($clubName);
            }
            if (!empty($clubUrl)) {
                $this->course->setClubURL($clubUrl);
            }
            if (!empty($secretKey)) {
                $this->course->setSecretKey($secretKey);
            }
            $result = CourseValidator::checkXml($xml);
            if ($result) {
                $errors[] = $result;
            }
            $result = CourseValidator::checkKml($kml);
            if ($result) {
                $errors[] = $result;
            }
            if (count($errors) > 0) {
                throw new CreateCourseException($errors);
            }
            try {
                $xmlContent = self::addXmlProperty($jsonData, $xml, "detection_radius", 'DetectionRadius', $this->course, 'setDetectionRadius', 'detectionRadiusToString');
                $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "discipline", 'Discipline', $this->course, "setDiscipline", 'disciplineToString');
                $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_type", 'CourseType', $this->course, "setCourseType", 'courseTypeToString');
                $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_format", 'CourseFormat', $this->course, "setCourseFormat", 'courseFormatToString');
                $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_mode", 'CourseValidationMode', $this->course, "setCourseMode", 'courseModeToString');
            } catch (Exception) {
                throw new CreateCourseException([CourseValidator::ERROR_XML_INVALID]);
            }
            $xmlContent = $this->xmlCourseFilter($xmlContent, $selectedXmlCourse);
            $xmlContent = $this->ifNeededAddLengthInXml($xmlContent);
            [$latitude, $longitude, $length] = $this->extractLocationAndLengthFromXml($xmlContent);
            if ($latitude == null || $longitude == null) {
                throw new CreateCourseException([CourseValidator::ERROR_XML_INVALID_LOCATION,]);
            }
            try {
                if ($startDate === null) {
                    $this->course->setStartDate(null);
                } else {
                    $sdt = DateTime::createFromFormat('U.u', number_format($startDate / 1000, 3, '.', ''));
                    if (!$sdt) {
                        $this->course->setStartDate(null);
                    } else {
                        $this->course->setStartDate($sdt);
                    }
                }
                if ($endDate === null) {
                    $this->course->setEndDate(null);
                } else {
                    $edt = DateTime::createFromFormat('U.u', number_format($endDate / 1000, 3, '.', ''));
                    if (!$edt) {
                        $this->course->setEndDate(null);
                    } else {
                        $this->course->setEndDate($edt);
                    }
                }
            } catch (Exception) {
                throw new CreateCourseException([CourseValidator::ERROR_INVALID_TIME_INTERVAL]);
            }
            $this->course->setXml($xmlContent);
            $this->course->setKml($kml);
            $this->course->setImage($imageFilename);
            $this->course->setCreator($this->user);
            $this->course->setLatitude($latitude);
            $this->course->setLongitude($longitude);
            $this->course->setLength($length);
            $this->course->setPrintable($printable ?? false);
            $this->course->setTouristic(self::containsTextTags($xmlContent));
            try {
                $entityManager->persist($this->course);
                $entityManager->flush();
            } catch (Exception) {
                throw new CreateCourseException([CourseValidator::ERROR_INTERNAL]);
            }
        } else { // Modify a course
            if ($name != null) {
                $this->course->setName($name);
            }
            if (!empty($clubName)) {
                $this->course->setClub($clubName);
            } else {
                $this->course->setClub(null);
            }
            if (!empty($clubUrl)) {
                $this->course->setClubURL($clubUrl);
            } else {
                $this->course->setClubURL(null);
            }
            if (!empty($secretKey)) {
                $this->course->setSecretKey($secretKey);
            } else {
                $this->course->setSecretKey(null);
            }
            if ($xml != null) {
                $errorCode = CourseValidator::checkXml($xml);
                if ($errorCode) {
                    $errors[] = $errorCode;
                    throw new CreateCourseException($errors);
                }
                try {
                    $xmlContent = self::addXmlProperty($jsonData, $xml, "detection_radius", 'DetectionRadius', $this->course, 'setDetectionRadius', 'detectionRadiusToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "discipline", 'Discipline', $this->course, "setDiscipline", 'disciplineToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_type", 'CourseType', $this->course, "setCourseType", 'courseTypeToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_format", 'CourseFormat', $this->course, "setCourseFormat", 'courseFormatToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_mode", 'CourseValidationMode', $this->course, "setCourseMode", 'courseModeToString');
                } catch (Exception) {
                    throw new CreateCourseException([CourseValidator::ERROR_XML_INVALID]);
                }
                $xmlContent = $this->xmlCourseFilter($xmlContent, $selectedXmlCourse);
                $xmlContent = $this->ifNeededAddLengthInXml($xmlContent);
                [$latitude, $longitude, $length] = $this->extractLocationAndLengthFromXml($xmlContent);
                if ($latitude == null || $longitude == null) {
                    throw new CreateCourseException([CourseValidator::ERROR_XML_INVALID_LOCATION]);
                }
                $this->course->setXml($xmlContent);
                $this->course->setLatitude($latitude);
                $this->course->setLongitude($longitude);
                $this->course->setLength($length);
                $this->course->setTouristic(self::containsTextTags($xmlContent));
            } else { // Case of stored XML file
                try {
                    $xmlContent = $this->course->getXml();
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "detection_radius", 'DetectionRadius', $this->course, 'setDetectionRadius', 'detectionRadiusToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "discipline", 'Discipline', $this->course, "setDiscipline", 'disciplineToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_type", 'CourseType', $this->course, "setCourseType", 'courseTypeToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_format", 'CourseFormat', $this->course, "setCourseFormat", 'courseFormatToString');
                    $xmlContent = self::addXmlProperty($jsonData, $xmlContent, "course_mode", 'CourseValidationMode', $this->course, "setCourseMode", 'courseModeToString');
                    $this->course->setXml($xmlContent);
                } catch (Exception) {
                    throw new CreateCourseException([CourseValidator::ERROR_XML_INVALID]);
                }
            }
            if ($kml != null) {
                $errorCode = CourseValidator::checkKml($kml);
                if ($errorCode) {
                    $errors[] = $errorCode;
                    throw new CreateCourseException($errors);
                }
                $this->course->setKml($kml);
            }
            try {
                if ($startDate === null) {
                    $this->course->setStartDate(null);
                } else {
                    $sdt = DateTime::createFromFormat('U.u', number_format($startDate / 1000, 3, '.', ''));
                    $this->course->setStartDate($sdt);
                }
                if ($endDate === null) {
                    $this->course->setEndDate(null);
                } else {
                    $edt = DateTime::createFromFormat('U.u', number_format($endDate / 1000, 3, '.', ''));
                    $this->course->setEndDate($edt);
                }
            } catch (Exception) {
                throw new CreateCourseException([CourseValidator::ERROR_INVALID_TIME_INTERVAL]);
            }
            $this->course->setPrintable($printable ?? false);
        }
        if ($jsonData["image"] !== null) {
            $temporaryPath = "upload/" . $this->course->getId() . "." . "tmp";
            $bytesCount = file_put_contents($temporaryPath, $imageData);
            if (!$bytesCount) {
                throw new CreateCourseException([CourseValidator::ERROR_IMAGE_TYPE_INVALID]);
            }
            if ($bytesCount > 4 * 1024 * 1024) {
                if (file_exists($temporaryPath)) {
                    unlink($temporaryPath);
                }
                throw new CreateCourseException([CourseValidator::ERROR_IMAGE_TOO_BIG]);
            }
            if (file_exists($this->course->getImage())) {
                unlink($this->course->getImage());
            }
            $finalPath = "upload/" . $this->course->getId() . "." . pathinfo($imageFilename)["extension"];
            rename($temporaryPath, $finalPath);
            $this->course->setImage($finalPath);
        }
        try {
            $entityManager->persist($this->course);
            $entityManager->flush();
            return $this->course->getId();
        } catch (Exception $e) {
            if (strpos($e->getMessage(), "Duplicate entry")) {
                $errors[] = CourseValidator::ERROR_COURSE_DUPLICATE;
            } else {
                if ($isCreationOfNewCourse && $this->course->getImage() && file_exists($this->course->getImage())) {
                    unlink($this->course->getImage());
                }
                $errors[] = CourseValidator::ERROR_INTERNAL;
            }
        }
        throw new CreateCourseException($errors);
    }

    public static function removeCoursePropertyInXml(string $xmlContent, string $elementName): string
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xmlContent);
        $domDocument = self::removeCoursePropertyInXml_($domDocument, $elementName);
        return $domDocument->saveXML();
    }

    public static function removeCoursePropertyInXml_(DOMDocument $domDocument, string $elementName): DOMDocument
    {
        $nodeToRemove = $domDocument->getElementsByTagname($elementName)->item(0);
        if (!empty($nodeToRemove)) {
            $nodeToRemove->parentNode->removeChild($nodeToRemove);
        }
        return $domDocument;
    }

    /**
     * @throws DOMException
     */
    public static function addCoursePropertyInXml(string $xmlContent, string $elementName, string $value): string
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xmlContent);
        $domDocument = self::addCoursePropertyInXml_($domDocument, $elementName, $value);
        return $domDocument->saveXML();
    }

    /**
     * @throws DOMException
     */
    public static function addCoursePropertyInXml_(DOMDocument $domDocument, string $elementName, string $value): DOMDocument
    {
        /** @var DOMElement $grandParentElementNode */
        $grandParentElementNode = $domDocument->getElementsByTagname('Map')->item(0);
        if ($grandParentElementNode == null) {
            $grandGrandParentElementNode = $domDocument->getElementsByTagname('RaceCourseData')->item(0);
            $grandParentElementNode = $domDocument->createElement('Map');
            $grandGrandParentElementNode->insertBefore($grandParentElementNode, $grandGrandParentElementNode->firstChild);
        }

        $parentElementNode = $grandParentElementNode->getElementsByTagname('Extensions')->item(0);
        if (empty($parentElementNode)) {
            $parentElementNode = $domDocument->createElement('Extensions');
            $comment = $domDocument->createComment("Added by Vikazimut");
            $parentElementNode->appendChild($comment);
            $grandParentElementNode->appendChild($parentElementNode);
        }
        $node2 = $domDocument->createElement($elementName, $value);
        $parentElementNode->appendChild($node2);
        return $domDocument;
    }

    private function xmlCourseFilter(string $xmlContent, int $idSelectedCourse): ?string
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xmlContent);
        $domNodeList = $domDocument->getElementsByTagname('Course');
        $nodeNumber = $domNodeList->length;
        if ($nodeNumber == 1) {
            return $xmlContent;
        }
        for ($i = $nodeNumber - 1; $i >= 0; $i--) {
            $domNode = $domNodeList->item($i);
            if ($i != $idSelectedCourse) {
                $domNode->parentNode->removeChild($domNode);
            }
        }
        return $domDocument->saveXML();
    }

    static function _computeTheoreticalLength(SimpleXMLElement $xmlContents): int
    {
        $legLengths = TrackStatistics::computeLegLengths($xmlContents);
        $length = 0;
        foreach ($legLengths as $l) {
            $length += $l;
        }
        return $length;
    }

    static function _simplexml_insert_after(SimpleXMLElement $insert, SimpleXMLElement $target): void
    {
        $target_dom = dom_import_simplexml($target);
        $insert_dom = $target_dom->ownerDocument->importNode(dom_import_simplexml($insert), true);
        if ($target_dom->nextSibling) {
            $target_dom->parentNode->insertBefore($insert_dom, $target_dom->nextSibling);
        } else {
            $target_dom->parentNode->appendChild($insert_dom);
        }
    }

    static function ifNeededAddLengthInXml(string $xmlContent): string
    {
        $xml = simplexml_load_string($xmlContent);
        $len = $xml->RaceCourseData->Course[0]->Length;
        if (empty($len)) {
            try {
                $len = self::_computeTheoreticalLength($xml);
                $node = $xml->RaceCourseData->Course[0]->Name;
                $insert = new SimpleXMLElement('<Length>' . $len . '</Length>');
                self::_simplexml_insert_after($insert, $node);
            } catch (Exception) {
            }
            return $xml->asXML();
        }
        return $xmlContent;
    }

    function extractLocationAndLengthFromXml(string $xmlContent): array
    {
        $contents = simplexml_load_string($xmlContent);
        $len = intval($contents->RaceCourseData->Course[0]->Length);
        $firstControlPointName = $this->getFirstControlPointName($contents);
        if ($firstControlPointName == null) {
            return [null, null, null];
        }
        $controlPoint = $this->getControlPointFromName($contents, $firstControlPointName);
        if ($controlPoint == null) {
            return [null, null, null];
        }
        $lat = $controlPoint->Position['lat'];
        $lng = $controlPoint->Position['lng'];
        // Workaround for OpenOrienteeringMapper issue: https://github.com/OpenOrienteering/mapper/issues/1947
        if ($lat == null && $lng == null) {
            $lat = $controlPoint->CourseControl['lat'];
            $lng = $controlPoint->CourseControl['lng'];
        }
        return [$lat, $lng, $len];
    }

    public static function getFirstControlPointName(?SimpleXMLElement $xmlContents): ?string
    {
        foreach ($xmlContents->RaceCourseData->Course->CourseControl as $children) {
            if (strcasecmp("Start", (string)$children["type"]) === 0) {
                return $children->Control;
            }
        }
        return null;
    }

    public static function getControlPointFromName($xmlContents, string $controlPointName): ?SimpleXMLElement
    {
        foreach ($xmlContents->RaceCourseData->Control as $control) {
            if (strcasecmp($control->Id, $controlPointName) === 0) {
                return $control;
            }
        }
        return null;
    }

    private static function containsTextTags(string $xmlContent): bool
    {
        $simplexml = simplexml_load_string($xmlContent);
        $checkpoints = [];
        $root = $simplexml->RaceCourseData;
        foreach ($root->Course->children() as $child) {
            if ($child->getName() === "CourseControl") {
                $id = (string)$child->Control;
                $checkpoints[] = $id;
            }
        }
        $root = $simplexml->RaceCourseData->Control;
        for ($i = 0; $i < $root->count(); $i++) {
            if (($root[$i]->text || ($root[$i]->Text)) && in_array($root[$i]->Id, $checkpoints)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @throws DOMException
     */
    private static function addXmlProperty($jsonData, string $xmlContent, string $propertyNameInForm, string $propertyNameInXml, Course $course, $setPropertyFunction, $propertyToStringFunction): string
    {
        $xmlContent = self::removeCoursePropertyInXml($xmlContent, $propertyNameInXml);
        call_user_func_array(array($course, $setPropertyFunction), array(null));
        $property = $jsonData[$propertyNameInForm];
        if ($property !== null && $property >= 0) {
            $propertyName = call_user_func_array(array($course, $propertyToStringFunction), array($property));
            $xmlContent = self::addCoursePropertyInXml($xmlContent, $propertyNameInXml, $propertyName);
            call_user_func_array(array($course, $setPropertyFunction), array($property));
        }
        return $xmlContent;
    }
}

