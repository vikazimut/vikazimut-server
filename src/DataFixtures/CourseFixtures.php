<?php

namespace App\DataFixtures;

use App\Entity\Course;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CourseFixtures extends Fixture implements DependentFixtureInterface
{
    public const LUC_COURSE_REFERENCE = 'course_luc';
    public const AUBIN_COURSE_REFERENCE = 'course_aubin';

    public function load(ObjectManager $manager): void
    {
        $course = new Course();
        $course->setCreator($this->getReference(UserFixtures::TEST_USER_REFERENCE));
        $course->setXml('<?xml version="1.0" encoding="UTF-8"?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2021-05-01T14:32:07.242+02:00" creator="OCAD 12.1.8.1187">
  <Event>
    <Name>Event</Name>
  </Event>
  <RaceCourseData>
    <Map>
      <Scale>4000</Scale>
      <!-- extent of ocad map-->
      <MapPositionTopLeft x="-263.1" y="215.1" unit="mm"/>
      <MapPositionBottomRight x="291.8" y="-357.0" unit="mm"/>
    </Map>
    <Control>
      <Id>S1</Id>
      <Position lng="-0.354340" lat="49.316959"/>
      <MapPosition x="32.0" y="55.4" unit="mm"/>
    </Control>
    <Control>
      <Id>31</Id>
      <Position lng="-0.347228" lat="49.318359"/>
      <MapPosition x="162.8" y="88.8" unit="mm"/>
    </Control>
    <Control>
      <Id>32</Id>
      <Position lng="-0.348080" lat="49.316169"/>
      <MapPosition x="144.8" y="28.6" unit="mm"/>
    </Control>
    <Control>
      <Id>33</Id>
      <Position lng="-0.347225" lat="49.313204"/>
      <MapPosition x="156.8" y="-54.4" unit="mm"/>
    </Control>
    <Control>
      <Id>34</Id>
      <Position lng="-0.351250" lat="49.313347"/>
      <MapPosition x="83.8" y="-47.3" unit="mm"/>
    </Control>
    <Control>
      <Id>35</Id>
      <Position lng="-0.350797" lat="49.311186"/>
      <MapPosition x="89.5" y="-107.7" unit="mm"/>
    </Control>
    <Control>
      <Id>36</Id>
      <Position lng="-0.350668" lat="49.318583"/>
      <MapPosition x="100.6" y="97.7" unit="mm"/>
    </Control>
    <Control>
      <Id>37</Id>
      <Position lng="-0.352960" lat="49.311026"/>
      <MapPosition x="50.0" y="-110.5" unit="mm"/>
    </Control>
    <Control>
      <Id>38</Id>
      <Position lng="-0.356716" lat="49.313257"/>
      <MapPosition x="-15.6" y="-45.6" unit="mm"/>
    </Control>
    <Control>
      <Id>39</Id>
      <Position lng="-0.357325" lat="49.314169"/>
      <MapPosition x="-25.5" y="-19.8" unit="mm"/>
    </Control>
    <Control>
      <Id>40</Id>
      <Position lng="-0.357232" lat="49.316928"/>
      <MapPosition x="-20.6" y="56.8" unit="mm"/>
    </Control>
    <Control>
      <Id>41</Id>
      <Position lng="-0.355650" lat="49.319523"/>
      <MapPosition x="11.2" y="127.7" unit="mm"/>
    </Control>
    <Control>
      <Id>42</Id>
      <Position lng="-0.355474" lat="49.317909"/>
      <MapPosition x="12.5" y="82.7" unit="mm"/>
    </Control>
    <Control>
      <Id>43</Id>
      <Position lng="-0.348966" lat="49.314841"/>
      <MapPosition x="127.1" y="-7.6" unit="mm"/>
    </Control>
    <Control>
      <Id>44</Id>
      <Position lng="-0.352874" lat="49.313882"/>
      <MapPosition x="55.0" y="-31.2" unit="mm"/>
    </Control>
    <Control>
      <Id>45</Id>
      <Position lng="-0.362883" lat="49.313389"/>
      <MapPosition x="-127.4" y="-37.2" unit="mm"/>
    </Control>
    <Control>
      <Id>46</Id>
      <Position lng="-0.361796" lat="49.314536"/>
      <MapPosition x="-106.3" y="-6.1" unit="mm"/>
    </Control>
    <Control>
      <Id>47</Id>
      <Position lng="-0.360033" lat="49.314678"/>
      <MapPosition x="-74.1" y="-3.5" unit="mm"/>
    </Control>
    <Control>
      <Id>48</Id>
      <Position lng="-0.355172" lat="49.314944"/>
      <MapPosition x="14.5" y="0.1" unit="mm"/>
    </Control>
    <Control>
      <Id>49</Id>
      <Position lng="-0.345489" lat="49.314994"/>
      <MapPosition x="190.4" y="-6.0" unit="mm"/>
    </Control>
    <Control>
      <Id>F1</Id>
      <Position lng="-0.354329" lat="49.316953"/>
      <MapPosition x="32.2" y="55.3" unit="mm"/>
    </Control>

    <Course>
      <Name>confirmé</Name>
      <Length>4870</Length>
      <Climb>0</Climb>
      <CourseControl type="Start">
        <Control>S1</Control>
      </CourseControl>
      <CourseControl type="Control">
        <Control>36</Control>
        <LegLength>322</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>31</Control>
        <LegLength>251</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>32</Control>
        <LegLength>251</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>33</Control>
        <LegLength>336</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>43</Control>
        <LegLength>222</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>49</Control>
        <LegLength>253</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>34</Control>
        <LegLength>457</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>35</Control>
        <LegLength>243</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>37</Control>
        <LegLength>158</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>38</Control>
        <LegLength>369</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>45</Control>
        <LegLength>449</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>46</Control>
        <LegLength>150</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>47</Control>
        <LegLength>129</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>39</Control>
        <LegLength>205</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>48</Control>
        <LegLength>179</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>40</Control>
        <LegLength>267</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>41</Control>
        <LegLength>311</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>42</Control>
        <LegLength>180</LegLength>
      </CourseControl>
      <CourseControl type="Finish">
        <Control>F1</Control>
        <LegLength>135</LegLength>
      </CourseControl>
    </Course>



  </RaceCourseData>
<Extensions><Creator></Creator></Extensions></CourseData>');
        $course->setImage("fake.jpeg");
        $course->setKml('<?xml version="1.0" encoding="UTF-8"?>
<!-- Generator: OCAD Version 12.1.8 -->
<kml xmlns="http://earth.google.com/kml/2.2">
<Folder>
        <name></name>
        <GroundOverlay>
                <name>tile_0_0.jpg</name>
                <drawOrder>75</drawOrder>
                <Icon>
                        <href>files/tile_0_0.jpg</href>
                        <viewBoundScale>0.75</viewBoundScale>
                </Icon>
                <LatLonBox>
                        <north>49.320576290</north>
                        <south>49.309456270</south>
                        <east>-0.344662641</east>
                        <west>-0.364440166</west>
                        <rotation>2.441000847</rotation>
                </LatLonBox>
        </GroundOverlay>
</Folder>
</kml>');
        $course->setName("Luc");
        $course->setCreatedAt(new DateTime("now"));
        $course->setLatitude("49.316959");
        $course->setLongitude("-0.354340");
        $course->setLength(4870);
        $course->setTouristic(false);
        $manager->persist($course);
        $this->addReference(self::LUC_COURSE_REFERENCE, $course);

        $course = new Course();
        $course->setCreator($this->getReference(UserFixtures::TEST_USER_REFERENCE));
        $course->setXml('<?xml version="1.0" encoding="UTF-8" ?>
<CourseData xmlns="http://www.orienteering.org/datastandard/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" iofVersion="3.0" createTime="2020-12-29T18:22:27.858+01:00" creator="OCAD 12.4.0.1684">
  <Event>
    <Name>Event</Name>
  </Event>
  <RaceCourseData>
    <Map>
      <Scale>4000</Scale>
      <!-- extent of ocad map-->
      <MapPositionTopLeft x="-184.2" y="77.0" unit="mm"/>
      <MapPositionBottomRight x="734.5" y="-1002.9" unit="mm"/>
    </Map>
    <Control>
      <Id>S1</Id>
      <Position lng="-0.389201" lat="49.328493" />
      <MapPosition x="162.5" y="-97.0" unit="mm" />
    </Control>
    <Control>
      <Id>31</Id>
      <Position lng="-0.387567" lat="49.329825" />
      <MapPosition x="193.8" y="-61.3" unit="mm" />
    </Control>
    <Control>
      <Id>32</Id>
      <Position lng="-0.389705" lat="49.331249" />
      <MapPosition x="156.6" y="-20.1" unit="mm" />
    </Control>
    <Control>
      <Id>33</Id>
      <Position lng="-0.393312" lat="49.331191" />
      <MapPosition x="91.1" y="-18.9" unit="mm" />
    </Control>
    <Control>
      <Id>34</Id>
      <Position lng="-0.395675" lat="49.328753" />
      <MapPosition x="45.2" y="-84.8" unit="mm" />
    </Control>
    <Control>
      <Id>35</Id>
      <Position lng="-0.395436" lat="49.326322" />
      <MapPosition x="46.7" y="-152.5" unit="mm" />
    </Control>
    <Control>
      <Id>36</Id>
      <Position lng="-0.391093" lat="49.326584" />
      <MapPosition x="125.9" y="-148.6" unit="mm" />
    </Control>
    <Control>
      <Id>37</Id>
      <Position lng="-0.393166" lat="49.330182" />
      <MapPosition x="92.5" y="-47.0" unit="mm" />
    </Control>
    <Control>
      <Id>38</Id>
      <Position lng="-0.391499" lat="49.331646" />
      <MapPosition x="124.5" y="-7.6" unit="mm" />
    </Control>
    <Control>
      <Id>39</Id>
      <Position lng="-0.389856" lat="49.329885" />
      <MapPosition x="152.3" y="-57.8" unit="mm" />
    </Control>
    <Control>
      <Id>40</Id>
      <Position lng="-0.390995" lat="49.328273" />
      <MapPosition x="129.7" y="-101.7" unit="mm" />
    </Control>
    <Control>
      <Id>41</Id>
      <Position lng="-0.387460" lat="49.327099" />
      <MapPosition x="192.5" y="-137.1" unit="mm" />
    </Control>
    <Control>
      <Id>42</Id>
      <Position lng="-0.387936" lat="49.328440" />
      <MapPosition x="185.4" y="-99.5" unit="mm" />
    </Control>
    <Control>
      <Id>F1</Id>
      <Position lng="-0.389159" lat="49.328781" />
      <MapPosition x="163.6" y="-89.1" unit="mm" />
    </Control>

    <Course>
      <Name>Saint Aubin</Name>
      <Length>3180</Length>
      <Climb>0</Climb>
      <CourseControl type="Start">
        <Control>S1</Control>
      </CourseControl>
      <CourseControl type="Control">
        <Control>31</Control>
        <LegLength>190</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>32</Control>
        <LegLength>222</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>33</Control>
        <LegLength>262</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>34</Control>
        <LegLength>321</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>35</Control>
        <LegLength>271</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>36</Control>
        <LegLength>317</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>37</Control>
        <LegLength>428</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>38</Control>
        <LegLength>203</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>39</Control>
        <LegLength>230</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>40</Control>
        <LegLength>197</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>41</Control>
        <LegLength>288</LegLength>
      </CourseControl>
      <CourseControl type="Control">
        <Control>42</Control>
        <LegLength>153</LegLength>
      </CourseControl>
      <CourseControl type="Finish">
        <Control>F1</Control>
        <LegLength>97</LegLength>
      </CourseControl>
    </Course>

  </RaceCourseData>
</CourseData>');
        $course->setImage("fake.jpeg");
        $course->setKml('<?xml version="1.0" encoding="UTF-8"?>
<!-- Generator: OCAD Version 12.4.0 -->
<kml xmlns="http://www.opengis.net/kml/2.2">
<Folder>
        <name></name>
        <GroundOverlay>
                <name>tile_0_0.jpg</name>
                <drawOrder>75</drawOrder>
                <Icon>
                        <href>files/tile_0_0.jpg</href>
                        <viewBoundScale>0.75</viewBoundScale>
                </Icon>
                <LatLonBox>
                        <north>49.332469380</north>
                        <south>49.324925520</south>
                        <east>-0.380839692</east>
                        <west>-0.397163799</west>
                        <rotation>2.464828175</rotation>
                </LatLonBox>
        </GroundOverlay>
</Folder>
</kml>');
        $course->setName("Aubin");
        $course->setCreatedAt(new DateTime("now"));
        $course->setLatitude("49.328493");
        $course->setLongitude("-0.389201");
        $course->setLength(3180);
        $course->setTouristic(false);
        $manager->persist($course);
        $this->addReference(self::AUBIN_COURSE_REFERENCE, $course);
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class
        ];
    }
}
