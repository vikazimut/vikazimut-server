<?php

namespace App\Tests\Model;

use App\Entity\Track;
use App\Model\CreateTrack;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertTrue;

class CreateTrackTest extends TestCase
{
    public function testIsRogaine_no_score()
    {
        $existingTrack = new Track();
        $existingTrack->setScore(-1);
        $newTrack = new Track();
        $existingTrack->setScore(0);
        $this->assertFalse(CreateTrack::isRogaine($newTrack, $existingTrack));
    }

    public function testIsRogaine_true()
    {
        $existingTrack = new Track();
        $existingTrack->setScore(1);
        $existingTrack->setPunches([
            ["punchTime" => 0, "controlPoint" => 0],
            ["punchTime" => 1, "controlPoint" => 1],
            ["punchTime" => 2, "controlPoint" => 2],
            ["punchTime" => -1, "controlPoint" => 3],
        ]);

        $newTrack = new Track();
        $newTrack->setScore(1);
        $newTrack->setPunches([
            ["punchTime" => -1, "controlPoint" => 0],
            ["punchTime" => -1, "controlPoint" => 1],
            ["punchTime" => 0, "controlPoint" => 2],
            ["punchTime" => 1, "controlPoint" => 3],
        ]);
        $this->assertTrue(CreateTrack::isRogaine($newTrack, $existingTrack));
    }

    public function testIsRogaine_false_not_continue()
    {
        $existingTrack = new Track();
        $existingTrack->setScore(1);
        $existingTrack->setPunches([
            ["punchTime" => 0, "controlPoint" => 0],
            ["punchTime" => 1, "controlPoint" => 1],
            ["punchTime" => 2, "controlPoint" => 2],
            ["punchTime" => -1, "controlPoint" => 3],
        ]);

        $newTrack = new Track();
        $newTrack->setScore(1);
        $newTrack->setPunches([
            ["punchTime" => -1, "controlPoint" => 0],
            ["punchTime" => -1, "controlPoint" => 1],
            ["punchTime" => 1, "controlPoint" => 2],
            ["punchTime" => 0, "controlPoint" => 3],
        ]);
        $this->assertFalse(CreateTrack::isRogaine($newTrack, $existingTrack));
    }

    public function testIsRogaine_false_common_checkpoints()
    {
        $existingTrack = new Track();
        $existingTrack->setScore(2);
        $existingTrack->setPunches([
            ["punchTime" => 0, "controlPoint" => 0],
            ["punchTime" => 1, "controlPoint" => 1],
            ["punchTime" => 3, "controlPoint" => 2],
            ["punchTime" => 2, "controlPoint" => 3],
            ["punchTime" => -1, "controlPoint" => 4],
        ]);

        $newTrack = new Track();
        $newTrack->setScore(2);
        $newTrack->setPunches([
            ["punchTime" => 2, "controlPoint" => 0],
            ["punchTime" => -1, "controlPoint" => 1],
            ["punchTime" => 0, "controlPoint" => 2],
            ["punchTime" => 1, "controlPoint" => 3],
            ["punchTime" => 2, "controlPoint" => 4],
        ]);
        $this->assertFalse(CreateTrack::isRogaine($newTrack, $existingTrack));
    }

    public function testGetLastPunchTime_1()
    {
        $track = new Track();
        $track->setPunches([
            ["punchTime" => 0, "controlPoint" => 0],
            ["punchTime" => 2, "controlPoint" => 1],
            ["punchTime" => -1, "controlPoint" => 2],
            ["punchTime" => 1, "controlPoint" => 3],
            ["punchTime" => -1, "controlPoint" => 4],
        ]);

        $this->assertEquals(2, CreateTrack::getLastPunchTime($track));
    }

    public function testGetLastPunchTime_2()
    {
        $track = new Track();
        $track->setScore(1);
        $track->setPunches([
            ["punchTime" => 0, "controlPoint" => 0],
            ["punchTime" => -1, "controlPoint" => 1],
            ["punchTime" => 2, "controlPoint" => 2],
            ["punchTime" => -1, "controlPoint" => 3],
            ["punchTime" => 4, "controlPoint" => 4],
        ]);

        $this->assertEquals(4, CreateTrack::getLastPunchTime($track));
    }

    public function testGetFirstPunchTime()
    {
        $track = new Track();
        $track->setScore(1);
        $track->setPunches([
            ["punchTime" => -1, "controlPoint" => 0],
            ["punchTime" => -1, "controlPoint" => 1],
            ["punchTime" => 3, "controlPoint" => 2],
            ["punchTime" => -1, "controlPoint" => 3],
            ["punchTime" => 1, "controlPoint" => 4],
        ]);

        $this->assertEquals(1, CreateTrack::getFirstPunchTime($track));
    }

    public function testMergePunches()
    {
        $track1 = new Track();
        $track1->setPunches([
            ["punchTime" => 0, "controlPoint" => 0],
            ["punchTime" => -1, "controlPoint" => 1],
            ["punchTime" => 2, "controlPoint" => 2],
            ["punchTime" => -1, "controlPoint" => 3],
            ["punchTime" => 4, "controlPoint" => 4],
        ]);
        $track2 = new Track();
        $track2->setPunches([
            ["punchTime" => 3, "controlPoint" => 0],
            ["punchTime" => -1, "controlPoint" => 1],
            ["punchTime" => -1, "controlPoint" => 2],
            ["punchTime" => 2, "controlPoint" => 3],
            ["punchTime" => 1, "controlPoint" => 4],
        ]);

        $newPunches = CreateTrack::mergePunches($track1->getPunches(), $track2->getPunches(), 4, 1);
        $this->assertEquals(0, $newPunches[0]["punchTime"]);
        $this->assertEquals(-1, $newPunches[1]["punchTime"]);
        $this->assertEquals(2, $newPunches[2]["punchTime"]);
        $this->assertEquals(5, $newPunches[3]["punchTime"]);
        $this->assertEquals(4, $newPunches[4]["punchTime"]);
    }

    public function testMergeTrace()
    {
        $trace1 = "0.0000,0.0000,0,0;0.0001,0.0001,1,0;0.0002,0.0002,2,0;0.0003,0.0003,3,0;";
        $trace2 = "-0.0001,-0.0001,2,0;-0.0002,-0.0002,3,0;-0.0003,-0.0003,4,0;";
        $baseTime1 = 3;
        $baseTime2 = 2;
        $newTrace = CreateTrack::mergeTrace($trace1, $trace2, $baseTime1, $baseTime2);
        $this->assertEquals("0,0,0,0;0.0001,0.0001,1,0;0.0002,0.0002,2,0;0.0003,0.0003,3,0;-0.0001,-0.0001,3,0;-0.0002,-0.0002,4,0;-0.0003,-0.0003,5,0;", $newTrace);
    }
}
