<?php


namespace App\Tests\Entity;

use App\Entity\Event;
use App\Entity\ParticipantMakeEventCourse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Exception\NotSupported;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ParticipantMakeEventCourseTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

//    /**
//     * @throws NotSupported
//     */
//    public function testRemove()
//    {
//        $nbParticipantMakeEventCourse = count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll());
//        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()[0];
//        $participantMakeEventCourse->remove($this->entityManager);
//        $this->assertSame($nbParticipantMakeEventCourse, count($this->entityManager->getRepository(ParticipantMakeEventCourse::class)->findAll()) + 1);
//    }

    /**
     * @throws NotSupported
     */
    public function testExtractFromTrackNbOverTimePenalty()
    {
        $championship = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getType() === 0) {
                $championship = $event;
            }
        }
        $suliac = null;
        foreach ($championship->getParticipants() as $participant) {
            if ($participant->getNickName() === "Suliac") {
                $suliac = $participant;
                break;
            }
        }
        $luc = null;
        foreach ($championship->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $luc = $eventCourse;
                break;
            }
        }
        echo $suliac->getId();
        echo ".";
        echo $luc->getId();
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $luc, "participant" => $suliac));
        $participantMakeEventCourse->updatePenaltiesAndPointsFromTrack();
        $this->assertSame(1, $participantMakeEventCourse->getOvertimeCount());
    }

    /**
     * @throws NotSupported
     */
    public function testExtractFromTrackNbMissingPunchPenalty()
    {
        $championship = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getType() === 0) {
                $championship = $event;
            }
        }
        $eric = null;
        foreach ($championship->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $eric = $participant;
            }
        }
        $luc = null;
        foreach ($championship->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $luc = $eventCourse;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $luc, "participant" => $eric));
        $participantMakeEventCourse->updatePenaltiesAndPointsFromTrack();

        $this->assertSame(1, $participantMakeEventCourse->getMispunchCount());
    }

    /**
     * @throws NotSupported
     */
    public function testExtractFromTrackScore()
    {
        $championship = null;
        foreach ($this->entityManager->getRepository(Event::class)->findAll() as $event) {
            if ($event->getType() === 1) {
                $championship = $event;
            }
        }
        $eric = null;
        foreach ($championship->getParticipants() as $participant) {
            if ($participant->getNickName() === "Eric") {
                $eric = $participant;
            }
        }
        $luc = null;
        foreach ($championship->getEventCourses() as $eventCourse) {
            if ($eventCourse->getCourse()->getName() === "Luc") {
                $luc = $eventCourse;
            }
        }
        $participantMakeEventCourse = $this->entityManager->getRepository(ParticipantMakeEventCourse::class)->find(array("eventCourse" => $luc, "participant" => $eric));
        $participantMakeEventCourse->updatePenaltiesAndPointsFromTrack();

        $this->assertSame(6, $participantMakeEventCourse->getScore());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testComputeTimeLimitPenaltyWhenLower()
    {
        $minutes = ParticipantMakeEventCourse::_computeOvertimeCount(1000, 1100);
        $this->assertSame(0, $minutes);
    }

    public function testComputeTimeLimitPenaltyWhenEquals()
    {
        $minutes = ParticipantMakeEventCourse::_computeOvertimeCount(1000, 1000);
        $this->assertSame(0, $minutes);
    }

    public function testComputeTimeLimitPenaltyWhenGreater()
    {
        $minutes = ParticipantMakeEventCourse::_computeOvertimeCount(1100, 1000);
        $this->assertSame(1, $minutes);
    }

    public function testComputeTimeLimitPenaltyWhen2()
    {
        $minutes = ParticipantMakeEventCourse::_computeOvertimeCount((2 * 60) * 1000, 60 * 1000);
        $this->assertSame(1, $minutes);
    }

    public function testComputeTimeLimitPenaltyWhen2plus()
    {
        $minutes = ParticipantMakeEventCourse::_computeOvertimeCount((2 * 60 + 1) * 1000, 60 * 1000);
        $this->assertSame(2, $minutes);
    }
}