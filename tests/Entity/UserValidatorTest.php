<?php

namespace App\Tests\Entity;

use App\Model\UserValidator;
use PHPUnit\Framework\TestCase;
use App\Entity\User;

class UserValidatorTest extends TestCase
{
    public function testTooLongUsername()
    {
        $this->assertFalse(UserValidator::checkUsername("aaaaaaaaaabbbbbbbbbbbcccccccccddddddddddeeeeeeeeeeffffffffffg"));
    }

    public function testValidUsername()
    {
        $this->assertTrue(UserValidator::checkUsername("Emmanuel DAMESTOY"));
    }

    public function testValidEmail()
    {
        $this->assertTrue(UserValidator::checkEmail("Emmanuel.Damestoy@ac-bordeaux.fr"));
    }

    public function testInvalidEmail()
    {
        $this->assertFalse(UserValidator::checkEmail("toto.t"));
    }

    public function testValidPhone()
    {
        $this->assertTrue(UserValidator::checkPhone(null));
        $this->assertTrue(UserValidator::checkPhone("+33111112"));
        $this->assertTrue(UserValidator::checkPhone("11 11 12"));
        $this->assertTrue(UserValidator::checkPhone("+11.11.12"));
        $this->assertFalse(UserValidator::checkPhone("+(11).11.12"));
    }

    public function testInvalidPhone()
    {
        $this->assertFalse(UserValidator::checkPhone("xxxxxx"));
        $this->assertFalse(UserValidator::checkPhone("1111111111111111111111111111111111111111111111111111111111111"));
    }

    public function testValidUser()
    {
        $user = new User();
        $user->setUsername("test");
        $user->setPassword("1\$OruA0TuUripYctziQ5f6uw\$GgmJW3gY7/yGnzU5gTSqtWghYVI7UK/Ai8QcKxwGxkc");
        $user->setEmail("t@t.t");
        $user->setPhone("+111 111 111 11");

        $this->assertTrue($user->isValid());
    }
}
